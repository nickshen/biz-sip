package com.bizmda.bizsip.app.checkrule;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.app.executor.script.MagicScriptHelper;
import org.ssssssss.script.MagicScriptContext;

import java.util.concurrent.Callable;

/**
 * @author 史正烨
 */
public class ServiceCheckRuleThread implements Callable<ServiceChcekRuleResult> {
    private final ServiceCheckRule serviceCheckRule;
    private final JSONObject jsonObject;

    public ServiceCheckRuleThread(JSONObject jsonObject, ServiceCheckRule serviceCheckRule) {
        this.serviceCheckRule = serviceCheckRule;
        this.jsonObject = jsonObject;
    }

    @Override
    public ServiceChcekRuleResult call() {
        MagicScriptContext context = new MagicScriptContext();
        context.set("data", this.jsonObject);
        Object result = MagicScriptHelper.executeScript(this.serviceCheckRule.getScript(), context);
        return ServiceChcekRuleResult.builder()
                .result(result)
                .build();
    }
}
