package com.bizmda.bizsip.app.executor;

import cn.hutool.core.util.ReflectUtil;
import cn.hutool.extra.spring.SpringUtil;
import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.*;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.UndeclaredThrowableException;
import java.util.Map;

/**
 * @author shizhengye
 */
@Slf4j
public class BeanAppExecutor extends AbstractAppExecutor {
    private Class<?> clazz;
    private Object springBean = null;

    public BeanAppExecutor(String serviceId, String type, Map<String,Object> configMap,boolean isAppYml) {
        super(serviceId, type, configMap);
        String className = (String)(isAppYml?
                configMap.get("class-name"):configMap.get("className"));
        try {
            log.info("初始化bean-service类App服务[{}]关联类: {}",serviceId,className);
            this.clazz = Class.forName(className);
        } catch (ClassNotFoundException e) {
            log.error("创建"+className+"类失败！",e);
        }
    }

    @Override
    public void init() {
        // 没有初始化内容
    }

    @Override
    public BizMessage<JSONObject> doAppService(BizMessage<JSONObject> message) throws BizException {
        log.debug("调用bean-service类App服务:\n{}",BizUtils.buildBizMessageLog(message));
        if (this.springBean == null) {
            this.springBean = SpringUtil.getBean(clazz);
        }
        String methodName = (String)message.getData().get("methodName");
        Method method = ReflectUtil.getMethodByName(this.clazz,methodName);
        if (method == null) {
            throw new BizException(BizResultEnum.APP_SERVICE_METHOD_NOT_FOUND,this.clazz.getName()+"的方法:" +methodName);
        }
        Object[] args = BizTools.convertJsonObject2MethodParameters(method,message.getData().get("params"),(JSONObject)message.getData().get("paramsTypes"));
        Object returnValue;
        try {
            log.debug("调用bean-service:{},{}({})",
                    this.springBean.getClass().getName(),methodName,args);
            returnValue = method.invoke(this.springBean,args);
        } catch (IllegalAccessException e) {
            log.error("调用bean-service出错",e);
            throw new BizException(BizResultEnum.OTHER_JAVA_CLASS_METHOD_ERROR,e);
        } catch (InvocationTargetException e) {
            Throwable t = e.getTargetException();
            if (t instanceof BizException) {
                throw (BizException) t;
            }
            else if (t instanceof UndeclaredThrowableException) {
                Throwable t1 = ((UndeclaredThrowableException) t).getUndeclaredThrowable();
                if (t1 instanceof BizException) {
                    throw (BizException) t1;
                }
            }
            log.error("调用bean-service出错",e);
            throw new BizException(BizResultEnum.OTHER_JAVA_CLASS_METHOD_ERROR,e);
        }
        JSONObject jsonObject = new JSONObject();
        Object resultObject = BizTools.methodReturnBean2Json(returnValue);
        jsonObject.set("result", resultObject);
        BizMessage<JSONObject> result =  BizTools.buildJsonObjectMessage(message,jsonObject);
        log.debug("bean-service类App服务返回:\n{}",BizUtils.buildBizMessageLog(result));
        return result;
    }
}
