package com.bizmda.bizsip.app.checkrule;

import lombok.Builder;
import lombok.Data;

/**
 * @author shizhengye
 */
@Data
@Builder
public class ControlRuleResult {
    private int index;
    private Object result;
    private String name;
}
