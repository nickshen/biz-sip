package com.bizmda.bizsip.app.client;

import lombok.Getter;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * @author shizhengye
 */
@Getter
public class SinkClientProxy<T> implements InvocationHandler {

    private final Class<T> mapperInterface;
    private final Map<Method, SinkClientMethod> methodCache;
    private final String sinkId;

    public SinkClientProxy(Class<T> mapperInterface, String sinkId) {
        this.mapperInterface = mapperInterface;
        this.methodCache = new HashMap<>();
        this.sinkId = sinkId;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (Object.class.equals(method.getDeclaringClass())) {
            return method.invoke(this, args);
        }
        final SinkClientMethod sinkClientMethod = cachedMapperMethod(method);

        return sinkClientMethod.execute(args);
    }

    private SinkClientMethod cachedMapperMethod(Method method) {
        return this.methodCache.computeIfAbsent(method,
                key -> new SinkClientMethod(key,this));
    }

}
