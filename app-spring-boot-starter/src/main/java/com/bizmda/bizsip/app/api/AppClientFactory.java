package com.bizmda.bizsip.app.api;

import com.bizmda.bizsip.app.client.DelayAppServiceClientProxy;
import com.bizmda.bizsip.app.client.SinkClientProxy;

import java.lang.reflect.Proxy;

/**
 * App层获取Sink层服务调用接口
 * @author shizhengye
 */
public class AppClientFactory {
    private AppClientFactory() {
        throw new IllegalStateException("Utility class");
    }
    /**
     * 获取Sink服务调用接口
     * @param tClass Sink服务接口类（要求是Interface）
     * @param sinkId 调用Sink的Sink ID
     * @param <T> 接口类泛型
     * @return 接口调用句柄
     */
    public static <T> T getSinkClient(Class<T> tClass,String sinkId) {
        final SinkClientProxy<T> sinkClientProxy = new SinkClientProxy<>(tClass,sinkId);
        return (T) Proxy.newProxyInstance(tClass.getClassLoader(), new Class[]{tClass}, sinkClientProxy);
    }

    /**
     * 获取App服务延迟调用接口
     * @param tClass 接口类（要求是Interface）
     * @param bizServiceId 调用聚合服务的Service ID
     * @param delayMilliseconds 延迟服务间隔时间（单位为毫秒）
     * @param <T> 接口类泛型
     * @return 接口调用句柄
     */
    public static <T> T getDelayAppServiceClient(Class<T> tClass, String bizServiceId, int... delayMilliseconds) {
        final DelayAppServiceClientProxy<T> delayAppServiceClientProxy = new DelayAppServiceClientProxy<>(tClass,bizServiceId,delayMilliseconds);
        return (T) Proxy.newProxyInstance(tClass.getClassLoader(), new Class[]{tClass}, delayAppServiceClientProxy);
    }
}
