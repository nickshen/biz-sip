package com.bizmda.bizsip.app.checkrule;

import lombok.Data;

import java.util.Map;

/**
 * @author shizhengye
 */
@Data
public class ServiceCheckRule {
    private String script;
    private String message;

    public ServiceCheckRule(Map<String,Object> map) {
        this.message = (String)map.get("message");
        String myScript = (String)map.get("script");
        if (myScript != null) {
            this.script = myScript;
            return;
        }
        String file = (String)map.get("file");
        if (file != null) {
            //TODO 载入文件
        }
    }
}
