package com.bizmda.bizsip.app.checkrule;

import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * @author shizhengye
 */
@Data
@Builder
public class FieldChcekRuleResult {
    private String field;
    private Object value;
    private String rule;
    private List<Object> args;
    private String message;
}
