package com.bizmda.bizsip.app.api;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;

/**
 * app-bean服务抽象接口
 * @author shizhengye
 */
public interface AppBeanInterface {
    /**
     * 执行APP服务
     * @param message 传入的消息
     * @return 返回的消息
     * @throws BizException Biz-SIP异常
     */
    JSONObject process(JSONObject message) throws BizException;
}
