package com.bizmda.bizsip.source.rest.controller;

import cn.hutool.json.JSONObject;
import lombok.Builder;
import lombok.Getter;

import java.util.Map;

/**
 * @author shizhengye
 */
@Builder
@Getter
public class RestSourceDTO {
    /**
     *  HTTP请求头Map
     */
    private Map<String,String> headerMap;
    /**
     * 上送的JSON报文
     */
    private JSONObject jsonObjectData;

    @Override
    public String toString() {
        return "RestSourceDTO{" +
                "headerMap=" + headerMap +
                ", jsonObjectData=" + jsonObjectData +
                '}';
    }
}
