package com.bizmda.bizsip.source.netty;

import com.bizmda.bizsip.source.api.SourceBeanInterface;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.util.CharsetUtil;

/**
 * @author 史正烨
 */
public class ServerChannelInitializer extends ChannelInitializer<SocketChannel> {
    private final SourceBeanInterface sourceService;

    public ServerChannelInitializer(SourceBeanInterface sourceService) {
        super();
        this.sourceService = sourceService;
    }
    @Override
    protected void initChannel(SocketChannel socketChannel) {
        //添加编解码
        socketChannel.pipeline().addLast("decoder", new StringDecoder(CharsetUtil.UTF_8));
        socketChannel.pipeline().addLast("encoder", new StringEncoder(CharsetUtil.UTF_8));
        socketChannel.pipeline().addLast(new NettyServerHandler(this.sourceService));
    }
}