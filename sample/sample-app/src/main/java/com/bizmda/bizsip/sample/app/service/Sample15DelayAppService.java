package com.bizmda.bizsip.sample.app.service;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.app.api.AppBeanInterface;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizResultEnum;
import com.bizmda.bizsip.common.BizUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author shizhengye
 */
@Slf4j
@Service
public class Sample15DelayAppService implements AppBeanInterface {
    @Override
    public JSONObject process(JSONObject message) throws BizException {
        log.info("延迟App服务调用第[{}]次: \n{}",
                BizUtils.getDelayRetryCount(),
                BizUtils.buildJsonLog(message));
        int maxRetryCount = message.getInt("maxRetryCount");
        String result = (String) message.get("result");
        if (BizUtils.getDelayRetryCount() >= maxRetryCount) {
            if ("success".equalsIgnoreCase(result)) {
                log.info("返回成功!");
                return message;
            }
            else {
                log.info("返回错误!");
                throw new BizException(BizResultEnum.OTHER_ERROR,"Sample15DelayAppService调用错误!");
            }
        }
        log.info("抛出延迟App服务重试异常,触发下一次调用...");
        throw new BizException(BizResultEnum.RETRY_DELAY_APP_SERVICE);
    }
}
