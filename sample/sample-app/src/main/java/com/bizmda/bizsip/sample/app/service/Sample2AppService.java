package com.bizmda.bizsip.sample.app.service;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.app.api.AppBeanInterface;
import com.bizmda.bizsip.app.api.AppClientFactory;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizMessage;
import com.bizmda.bizsip.common.BizMessageInterface;
import com.bizmda.bizsip.common.BizUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author shizhengye
 */
@Slf4j
@Service
public class Sample2AppService implements AppBeanInterface {
    private final BizMessageInterface sinkInterface = AppClientFactory
            .getSinkClient(BizMessageInterface.class,"hello-sink");

    @Override
    public JSONObject process(JSONObject message) throws BizException {
        log.debug("收到请求数据:\n{}", BizUtils.buildJsonLog(message));
        BizMessage<JSONObject> bizMessage = sinkInterface.call(message);
//        try {
//            Thread.sleep(4000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        return bizMessage.getData();
    }
}
