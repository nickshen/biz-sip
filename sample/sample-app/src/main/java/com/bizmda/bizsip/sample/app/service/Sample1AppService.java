package com.bizmda.bizsip.sample.app.service;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.app.api.AppBeanInterface;
import com.bizmda.bizsip.common.BizUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author shizhengye
 */
@Slf4j
@Service
public class Sample1AppService implements AppBeanInterface {
    @Override
    public JSONObject process(JSONObject message) {
        log.debug("收到请求数据:\n{}", BizUtils.buildJsonLog(message));
        return message;
    }
}
