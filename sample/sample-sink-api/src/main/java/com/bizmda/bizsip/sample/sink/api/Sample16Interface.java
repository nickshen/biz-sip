package com.bizmda.bizsip.sample.sink.api;

import com.bizmda.bizsip.common.BizException;

import java.util.List;

/**
 * @author shizhengye
 */
public interface Sample16Interface {
    String doService1(String arg1);

    void doService2(String arg1, int arg2);

    String doService1Exception(String arg1) throws BizException;

    CustomerDTO queryCustomerDTO(String customerId);

    AccountDTO[] queryAccounts(AccountDTO accountDTO);

    List<AccountDTO> queryAccountList(AccountDTO accountDTO);

    String notify(int maxRetryNum, String result) throws BizException;

    void saveAll(AccountDTO[] accountDtos);

    void saveAllList(List<AccountDTO> accountDTOList);

    void testParamsType(Object arg1, Object arg2, Object arg3);
}
