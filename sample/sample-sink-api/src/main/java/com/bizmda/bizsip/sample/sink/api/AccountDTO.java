package com.bizmda.bizsip.sample.sink.api;

import lombok.Builder;
import lombok.Data;

/**
 * @author shizhengye
 */
@Data
@Builder
public class AccountDTO {
    private String account;
    private Long balance;
}
