package com.bizmda.bizsip.sample.sink.controller;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizMessage;
import com.bizmda.bizsip.converter.Converter;
import com.bizmda.bizsip.sink.connector.Connector;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletResponse;

/**
 * @author 史正烨
 */
@Slf4j
public class Sink1Controller {
    private final Converter converter = Converter.getSinkConverter("sink1");
    private final Connector connector = Connector.getSinkConnector("sink1");

    public Sink1Controller() throws BizException {
        // 无操作
    }

    @PostMapping(value = "/sink1", consumes = "application/json", produces = "application/json")
    public BizMessage<JSONObject> doService(@RequestBody BizMessage<JSONObject> inMessage, HttpServletResponse response) {
        try {
            assert this.converter != null;
            byte[] packedMessage = this.converter.pack(inMessage.getData());
            assert this.connector != null;
            byte[] returnMessage = this.connector.process(packedMessage);
            JSONObject jsonObject = this.converter.unpack(returnMessage);
            return BizMessage.buildSuccessMessage(inMessage,jsonObject);
        } catch (BizException e) {
            log.error("服务端适配器执行出错",e);
            return BizMessage.buildFailMessage(inMessage,e);
        }
    }
}