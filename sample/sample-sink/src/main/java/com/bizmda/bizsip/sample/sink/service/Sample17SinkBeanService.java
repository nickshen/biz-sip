package com.bizmda.bizsip.sample.sink.service;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizUtils;
import com.bizmda.bizsip.sink.api.AbstractSinkService;
import com.bizmda.bizsip.sink.api.SinkBeanInterface;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author 史正烨
 */
@Slf4j
@Service
public class Sample17SinkBeanService extends AbstractSinkService implements SinkBeanInterface {
    @Override
    public JSONObject process(JSONObject inJsonObject) throws BizException {
        log.debug("收到报文:\n{}", BizUtils.buildJsonLog(inJsonObject));
        byte[] inBytes = this.converter.pack(inJsonObject);
        log.debug("打包后报文:\n{}",BizUtils.buildHexLog(inBytes));
        byte[] outBytes = this.connector.process(inBytes);
        log.debug("Connector返回报文:\n{}",BizUtils.buildHexLog(outBytes));
        JSONObject outJsonObject = this.converter.unpack(outBytes);
        log.debug("解包后报文:\n{}",BizUtils.buildJsonLog(outJsonObject));
        return outJsonObject;
    }
}
