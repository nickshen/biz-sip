package com.bizmda.bizsip.sample.sink;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;

/**
 * @author 史正烨
 */
@Slf4j
@SpringBootApplication
@ComponentScan(basePackages={"cn.hutool.extra.spring","com.bizmda.bizsip.sample"})
@Import(cn.hutool.extra.spring.SpringUtil.class)
public class SampleSinkApplication {
    public static void main(String[] args) {
        SpringApplication.run(SampleSinkApplication.class, args);
    }
}