package com.bizmda.bizsip.sample.sink.service;

import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizUtils;
import com.bizmda.bizsip.sink.connector.ByteProcessInterface;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @author 史正烨
 */
@Slf4j
@Service
public class EchoConnectorService implements ByteProcessInterface {
    @Override
    public byte[] process(byte[] inMessage) throws BizException {
        log.debug("收到报文:\n{}", BizUtils.buildHexLog(inMessage));
        return inMessage;
    }
}
