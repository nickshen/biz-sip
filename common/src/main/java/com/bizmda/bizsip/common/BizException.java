package com.bizmda.bizsip.common;

/**
 * Biz-SIP平台统一应用异常类
 * @author 史正烨
 */
public class BizException extends Exception {
    private final int code;
    private final String extMessage;

    /**
     * 基于异常枚举的构造方法
     * @param resultEnumInterface 异常枚举
     */
    public BizException(ResultEnumInterface resultEnumInterface) {
        super((resultEnumInterface.getMessage()));
        this.code = resultEnumInterface.getCode();
        this.extMessage = null;
    }

    /**
     * 基于异常枚举和异常类的构造方法
     * @param resultEnumInterface 异常枚举
     * @param e 异常类
     */
    public BizException(ResultEnumInterface resultEnumInterface, Throwable e) {
        super(resultEnumInterface.getMessage(),e);
        this.code = resultEnumInterface.getCode();
        this.extMessage = e.getMessage();
    }

    /**
     * 基于错误码和错误信息的构造方法
     * @param code 错误码
     * @param message 错误信息
     */
    public BizException(int code,String message) {
        super(message);
        this.code = code;
        this.extMessage = null;
    }

    /**
     * 基于异常枚举和扩展错误信息的构造方法
     * @param resultEnum 异常枚举
     * @param extMessage 扩展错误信息
     */
    public BizException(ResultEnumInterface resultEnum, String extMessage) {
        super((resultEnum.getMessage()));
        this.code = resultEnum.getCode();
        this.extMessage = extMessage;
    }

    /**
     * 基于异常枚举、异常和扩展错误信息的构造方法
     * @param resultEnum 异常枚举
     * @param e 异常
     * @param extMessage 扩展错误信息
     */
    public BizException(ResultEnumInterface resultEnum, Throwable e, String extMessage) {
        super(resultEnum.getMessage(),e);
        this.code = resultEnum.getCode();
        this.extMessage = extMessage;
    }

    /**
     * 基于错误码、错误信息和扩展错误信息的构造方法
     * @param code 错误码
     * @param message 错误信息
     * @param extMessage 扩展错误信息
     */
    public BizException(int code,String message,String extMessage) {
        super(message);
        this.code = code;
        this.extMessage = extMessage;
    }

    /**
     * 基于平台标准报文的构造方法
     * @param bizMessage 平台标准报文
     */
    public BizException(BizMessage<?> bizMessage) {
        super(bizMessage.getMessage());
        this.code = bizMessage.getCode();
        this.extMessage = bizMessage.getExtMessage();
    }

    /**
     * 返回异常错误码
     * @return 异常错误码
     */
    public int getCode() {
        return code;
    }

    /**
     * 返回异常错误扩展信息
     * @return 异常错误扩展信息
     */
    public String getExtMessage() {
        return extMessage;
    }

    /**
     * 判断是否为App延迟服务重试异常
     * @return 是否为App延迟服务重试异常
     */
    public boolean isRetryDelayAppService() {
        return (this.code == BizResultEnum.RETRY_DELAY_APP_SERVICE.getCode());
    }
}
