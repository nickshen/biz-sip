package com.bizmda.bizsip.converter;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizTools;

/**
 * @author 史正烨
 */
public class SimpleJsonConverter extends AbstractConverter {
    @Override
    protected JSONObject biz2json(JSONObject inMessage) throws BizException {
        return inMessage;
    }

    @Override
    protected byte[] json2adaptor(JSONObject inMessage) throws BizException {
        return BizTools.getBytes(JSONUtil.toJsonStr(inMessage));
    }

    @Override
    protected JSONObject adaptor2json(byte[] inBytes) throws BizException {
        return JSONUtil.parseObj(BizTools.getString(inBytes));
    }

    @Override
    protected JSONObject json2biz(JSONObject inMessage) throws BizException {
        return inMessage;
    }
}
