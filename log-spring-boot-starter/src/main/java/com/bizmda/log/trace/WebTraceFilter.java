package com.bizmda.log.trace;

import cn.hutool.core.text.CharSequenceUtil;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.core.annotation.Order;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * web过滤器，生成日志链路追踪id，并赋值MDC
 *
 * @author zlt
 * Blog: https://zlt2000.gitee.io
 * Github: https://github.com/zlt2000
 */
@ConditionalOnClass(value = {HttpServletRequest.class, OncePerRequestFilter.class})
@Order(value = MDCTraceUtils.FILTER_ORDER)
@Slf4j
public class WebTraceFilter extends OncePerRequestFilter {
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                    FilterChain filterChain) throws IOException, ServletException {
        try {
            String traceId = request.getHeader(MDCTraceUtils.TRACE_ID_HEADER);
            if (CharSequenceUtil.isEmpty(traceId)) {
                String istioTracdId = request.getHeader(MDCTraceUtils.ISTIO_X_B3_TRACEID);
                if (CharSequenceUtil.isEmpty(istioTracdId)) {
//                    traceId由Biz-SIP平台生成
//                    MDCTraceUtils.addTraceId();
                }
                else {
                    MDCTraceUtils.putTraceId(istioTracdId);
                }
            } else {
                MDCTraceUtils.putTraceId(traceId);
            }
            String value;
            for(String istioKey:MDCTraceUtils.ISTIO_TRACE_HEADERS) {
                value = request.getHeader(istioKey);
                if (!CharSequenceUtil.isEmpty(value)) {
                    MDC.put(istioKey,value);
                }
            }
            filterChain.doFilter(request, response);
        } finally {
            MDCTraceUtils.removeTraceId();
        }
    }
}
