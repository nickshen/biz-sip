package com.bizmda.bizsip.sink.config;

import cn.hutool.core.text.CharSequenceUtil;
import cn.hutool.core.util.URLUtil;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.common.BizMessage;
import com.bizmda.bizsip.common.BizResultEnum;
import com.bizmda.bizsip.config.AbstractSinkConfig;
import com.bizmda.bizsip.config.RabbitmqSinkConfig;
import com.bizmda.bizsip.config.RestSinkConfig;
import com.bizmda.bizsip.config.SinkConfigMapping;
import com.bizmda.bizsip.sink.processor.*;
import com.bizmda.log.trace.RestTemplateTraceInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 史正烨
 */
@Slf4j
@Configuration
public class SinkConfiguration {
    @Value("${bizsip.config-path:#{null}}")
    private String configPath;
    @Value("${bizsip.sink-id:#{null}}")
    private String sinkId;
    @Value("${bizsip.rabbitmq-log:#{null}}")
    private String rabbitmqLog;
    @Value("${spring.application.name}")
    private String applicationName;
    @Value("${bizsip.rest.connection.connection-request-timeout:-1}")
    private int connectionRequestTimeout;
    @Value("${bizsip.rest.connection.connect-timeout:-1}")
    private int connectTimeout;
    @Value("${bizsip.rest.connection.read-timeout:-1}")
    private int readTimeout;
    @Value("${bizsip.rest.connection.max-total:10}")
    private int maxTotal;
    @Value("${bizsip.rest.connection.default-max-per-route:5}")
    private int defaultMaxPerRoute;

    @Autowired
    private CachingConnectionFactory connectionFactory;
    @Autowired
    private ApplicationContext applicationContext;
    @Autowired
    private RequestMappingHandlerMapping requestMappingHandlerMapping;
    private Map<String, DirectExchange> sinkExchangeMap;

    @Bean
    public SinkConfigMapping sinkConfigMapping() throws BizException {
        return new SinkConfigMapping(this.configPath);
    }

    @Bean
    @ConditionalOnProperty(name = "spring.rabbitmq.host")
    public RabbitTemplate rabbitTemplate() {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(new Jackson2JsonMessageConverter());
        return rabbitTemplate;
    }

    @Bean
    public HttpComponentsClientHttpRequestFactory bizsipHttpRequestFactory() {
        Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory>create()
                .register("http", PlainConnectionSocketFactory.getSocketFactory())
                .register("https", SSLConnectionSocketFactory.getSocketFactory()).build();

        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager(registry);
        // 设置最大连接池的数量
        connectionManager.setMaxTotal(this.maxTotal);
        // 每个主机的最大并发量，route是指域名。--对MaxTotal的细化
        connectionManager.setDefaultMaxPerRoute(this.defaultMaxPerRoute);

        RequestConfig requestConfig = RequestConfig.custom()
                // 数据返回超时时间
                .setSocketTimeout(this.readTimeout)
                // 连接超时时间
                .setConnectTimeout(this.connectTimeout)
                // 从连接池中获取连接的超时时间
                .setConnectionRequestTimeout(this.connectionRequestTimeout)
                .build();

        CloseableHttpClient closeableHttpClient = HttpClientBuilder.create()
                .setDefaultRequestConfig(requestConfig)
                .setConnectionManager(connectionManager)
                .build();

        HttpComponentsClientHttpRequestFactory httpComponentsClientHttpRequestFactory
                = new HttpComponentsClientHttpRequestFactory(closeableHttpClient);
        return httpComponentsClientHttpRequestFactory;
    }

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate()
    {
        RestTemplate restTemplate = new RestTemplate(bizsipHttpRequestFactory());
        log.info("RestTemplate调用增加日志跟踪能力拦截器");
        restTemplate.setInterceptors(Collections.singletonList(new RestTemplateTraceInterceptor()));
        return restTemplate;
    }

    @Bean
    public Map<String, DirectExchange> sinkExchangeMap(SinkConfigMapping sinkConfigMapping) throws BizException {
        String[] sinkIds;
        if (CharSequenceUtil.isEmpty(this.sinkId)) {
            sinkIds = new String[0];
        } else {
            sinkIds = this.sinkId.split(",");
        }
        this.sinkExchangeMap = new HashMap<>();
        for (String mySinkId : sinkIds) {
            AbstractSinkConfig sinkConfig = sinkConfigMapping.getSinkConfigMap().get(mySinkId);
            if (sinkConfig == null) {
                log.error("Sink[{}]没有在sink.yml中定义，装载失败!", mySinkId);
                continue;
            }
            log.info("初始化Sink服务[{}]: {}", mySinkId, sinkConfig);

            switch (sinkConfig.getType()) {
                case AbstractSinkConfig.TYPE_REST:
                    log.info("Sink服务[{}],初始化同步调用接口(RestController)", mySinkId);
                    this.regiestSinkController(sinkConfig);
                    break;
                case AbstractSinkConfig.TYPE_RABBITMQ:
                    log.info("Sink服务[{}],初始化异步调用接口(RabbitmqListener)", mySinkId);
                    this.regiestSinkMessageListener(sinkConfig);
                    break;
                default:
            }

        }
        return this.sinkExchangeMap;
    }


    private void regiestSinkController(AbstractSinkConfig sinkConfig) throws BizException {
        if (!(sinkConfig instanceof RestSinkConfig)) {
            return;
        }
        RestSinkConfig restSinkConfig = (RestSinkConfig) sinkConfig;
        String path1 = "http://" + this.applicationName + "/" + restSinkConfig.getId();
        if (!path1.equals(restSinkConfig.getUrl())) {
            log.warn("sink.yml中sink的url值配置不规范:{}(建议配置为{})",restSinkConfig.getUrl(),path1);
        }
        String path = URLUtil.getPath(restSinkConfig.getUrl());
        RequestMappingInfo info = RequestMappingInfo.paths(path).consumes("application/json").produces("application/json").methods(RequestMethod.POST).build();
        Method method;
        try {
            method = SinkRestController.class.getMethod("service", BizMessage.class);
        } catch (NoSuchMethodException e) {
            log.error("SinkRestController获取方法出错!", e);
            throw new BizException(BizResultEnum.OTHER_ERROR, e);
        }
        SinkRestController sinkRestController = new SinkRestController(sinkConfig);
        requestMappingHandlerMapping.registerMapping(info, sinkRestController, method);
        log.info("注册Sink服务[{}]同步接口: path={}", sinkConfig.getId(), path);
    }


    private void regiestSinkMessageListener(AbstractSinkConfig sinkConfig) throws BizException {
        ConfigurableApplicationContext context = (ConfigurableApplicationContext) applicationContext;
        RabbitmqSinkConfig rabbitmqSinkConfig = (RabbitmqSinkConfig) sinkConfig;
        SinkMessageListener sinkMessageListener = new SinkMessageListener(sinkConfig, rabbitTemplate(), this.rabbitmqLog);
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer(connectionFactory);
        Environment env = applicationContext.getEnvironment();
        container.setConcurrentConsumers(env.getProperty("spring.rabbitmq.listener.simple.concurrency", int.class));
        container.setMaxConcurrentConsumers(env.getProperty("spring.rabbitmq.listener.simple.max-concurrency", int.class));
        container.setPrefetchCount(env.getProperty("spring.rabbitmq.listener.simple.prefetch", int.class));
        Queue queue = new Queue(rabbitmqSinkConfig.getQueue(), true, false, false);
        context.getBeanFactory().registerSingleton("rabbitmq-queue-" + rabbitmqSinkConfig.getId(), queue);
        DirectExchange directExchange = this.sinkExchangeMap.get(rabbitmqSinkConfig.getExchange());
        if (directExchange == null) {
            directExchange = new DirectExchange(rabbitmqSinkConfig.getExchange(), true, false);
            context.getBeanFactory().registerSingleton("rabbitmq-exchange-" + rabbitmqSinkConfig.getExchange(), directExchange);
            this.sinkExchangeMap.put(rabbitmqSinkConfig.getExchange(), directExchange);
        }
        Binding binding = BindingBuilder.bind(queue).to(directExchange).with(rabbitmqSinkConfig.getRoutingKey());
        context.getBeanFactory().registerSingleton("rabbitmq-binding-" + rabbitmqSinkConfig.getId(), binding);
        container.setQueues(queue);
        container.setAcknowledgeMode(AcknowledgeMode.AUTO);
        container.setMessageListener(sinkMessageListener);
        context.getBeanFactory().registerSingleton("rabbitmq-listener-" + rabbitmqSinkConfig.getId(), container);

        log.info("注册Sink服务[{}]异步接口: queue={},exchange={},key={}",
                sinkConfig.getId(), rabbitmqSinkConfig.getQueue(),
                rabbitmqSinkConfig.getExchange(), rabbitmqSinkConfig.getRoutingKey());
    }
}
