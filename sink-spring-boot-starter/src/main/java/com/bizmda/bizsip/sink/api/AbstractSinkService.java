package com.bizmda.bizsip.sink.api;

import com.bizmda.bizsip.converter.Converter;
import com.bizmda.bizsip.sink.connector.Connector;

/**
 * Sink服务类的抽象类
 * @author shizhengye
 */
public abstract class AbstractSinkService {
    protected Converter converter;
    protected Connector connector;

    /**
     * 设置消息转换器
     * @param converter 消息转换器
     */
    public void setConverter(Converter converter) {
        this.converter = converter;
    }

    /**
     * 设置通讯连接器
     * @param connector 通讯连接器
     */
    public void setConnector(Connector connector) {
        this.connector = connector;
    }
}
