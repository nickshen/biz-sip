package com.bizmda.bizsip.sink.processor;

import cn.hutool.json.JSONObject;
import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.config.AbstractSinkConfig;
import com.bizmda.bizsip.converter.Converter;
import com.bizmda.bizsip.sink.connector.Connector;
import lombok.extern.slf4j.Slf4j;

@Slf4j
/**
 * @author shizhengye
 */
public abstract class AbstractSinkProcessor {
    protected AbstractSinkConfig sinkConfig;
    protected Converter converter;
    protected Connector connector;

    protected AbstractSinkProcessor(AbstractSinkConfig sinkConfig) {
        this.sinkConfig = sinkConfig;
        try {
            this.converter = Converter.getSinkConverter(sinkConfig.getId());
            this.connector = Connector.getSinkConnector(sinkConfig.getId());
        } catch (BizException e) {
            log.error("Sink Porcessor初始化出错!",e);
        }
    }

    /**
     * Sink服务处理方法
     * @param inMessage 传入的JSONObject对象
     * @return JSONObject对象
     * @throws BizException BizException
     */
    abstract JSONObject process(JSONObject inMessage) throws BizException;
}
