package com.bizmda.bizsip.sink.connector;

import com.bizmda.bizsip.common.BizException;
import com.bizmda.bizsip.config.AbstractSinkConfig;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 史正烨
 */
@Data
public abstract class AbstractSinkConnector {
    protected static final Map<String,Object> CONNECTOR_TYPE_MAP = new HashMap<>();
    static {
        CONNECTOR_TYPE_MAP.put("service", ServiceSinkConnector.class);
        CONNECTOR_TYPE_MAP.put("netty", NettySinkConnector.class);
        CONNECTOR_TYPE_MAP.put("rabbitmq", RabbitmqSinkConnector.class);
        CONNECTOR_TYPE_MAP.put("http-post", HttpPostSinkConnector.class);
    }

    private String type;

    /**
     * Sink Connector初始化
     * @param sinkConfig Sink配置参数
     * @throws BizException BizException异常
     */
    public void init(AbstractSinkConfig sinkConfig) throws BizException {
    }
}
