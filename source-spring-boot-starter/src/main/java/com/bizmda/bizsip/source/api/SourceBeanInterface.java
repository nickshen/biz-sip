package com.bizmda.bizsip.source.api;

import com.bizmda.bizsip.common.BizException;

/**
 * Source服务接口
 * @author shizhengye
 */
public interface SourceBeanInterface {
    /**
     * Source模块类处理方法
     * @param data 传入Source服务的数据
     * @return Source服务返回数据
     * @throws BizException Biz-SIP异常
     */
    Object process(Object data) throws BizException;
}
